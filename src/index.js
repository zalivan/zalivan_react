import 'core-js/fn/object/assign';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/Main';
import Edit from './components/Edit/Edit';
import {BrowserRouter as Router, Route} from 'react-router-dom'

ReactDOM.render(
  (<Router>
    <div>
      <Route exact path="/" component={App}/>
      <Route path="/:id" component={Edit}/>
    </div>
  </Router>)
  , document.getElementById('app'));
