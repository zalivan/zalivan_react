import React, {Component} from 'react';

export default class RenderActions extends Component {
  render() {
    return (
      <td>
        <button className="btn btn-warning button_edit" onClick={this.handleRatingEdit.bind(this)} data-id = {this.props.rating.ratingId}>
          Edit rating
        </button>
        <button className="btn btn-danger" onClick={this.handleRatingDelete.bind(this)} data-id = {this.props.rating.ratingId}>
          Delete rating
        </button>
      </td>
    )
  }

  handleRatingDelete = (ev) => {
    const idCompetition = +ev.target.getAttribute('data-id');
    this.props.deleteRating(idCompetition);
  };

  handleRatingEdit = (ev) => {
    const idCompetition = +ev.target.getAttribute('data-id');
    this.props.editRating(idCompetition);
  }
}
