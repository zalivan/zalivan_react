require('../../styles/Edit.css');

import React from 'react'

export default class RenderAthlete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,
      name: props.Athlete.name,
      sport: props.Athlete.sport
    }
  }

  render() {
    let {Athlete} = this.props;
    let tempEdit = '';
    if ( this.state.isEdit) {
      tempEdit = (
        <div>
          <input
            className="form-control edit_form col-md-6 center"
            defaultValue={this.state.name || Athlete.name}
            onChange={this.handleEditName.bind(this)}
            onKeyDown={this.keyDownEnter}
            ref="changeName"
          />
          <input
            className="form-control edit_form col-md-6 center"
            defaultValue={this.state.sport || Athlete.sport}
            onChange={this.handleEditSport.bind(this)}
            onKeyDown={this.keyDownEnterSport}
            ref="changeSport"
          />
          <button
            className="btn btn-success edit_form"
            onClick={this.handleEditClick.bind(this)}>
            Ok
          </button>
          <button
            className="btn btn-warning edit_form btn_cancel"
            onClick={this.handleEditCancel.bind(this)}>
            Cancel
          </button>
        </div>
      )
    } else {
      tempEdit = (
        <div>
          <button className="btn btn-warning edit_form" onClick={this.handleEdit.bind(this)}>Edit athlete</button>
          <br/>
        </div>
      )
    }

    return (
      <div>
        {tempEdit}
      </div>
    )
  }

  handleEditName(ev) {
    this.setState({
      name: ev.target.value || ''
    })
  }

  handleEditSport(ev) {
    this.setState({
      sport: ev.target.value
    })
  }

  handleEdit() {
    this.setState({
      isEdit: true
    });
    this.props.editAthleteEditState();
  }

  handleEditClick() {
    const Name = this.refs.changeName;
    const Sport = this.refs.changeSport;

    Name.classList.remove('error');
    Sport.classList.remove('error');

    if((Name.value.trim() === '')&&(Name.value.trim().replace(/\s0-9/g, '') === '')) {
      Name.classList.add('error');
    } else if ((Sport.value.trim() === '')&&(Sport.value.trim().replace(/\s0-9/g, '') === '')) {
      Sport.classList.add('error');
    } else {
      this.props.editAthlete(Name.value, Sport.value);
      this.setState({
        isEdit: false
      })
    }
  }

  keyDownEnter = (ev) => {
    if (ev.keyCode === 13) {
      this.handleEditClick()
    } else {
      this.setState({
        name: ev.target.value
      });
    }
  };

  keyDownEnterSport = (ev) => {
    if (ev.keyCode === 13) {
      this.handleEditClick()
    } else {
      this.setState({
        sport: ev.target.value
      });
    }
  };

  handleEditCancel() {
    this.setState({
      isEdit: false,
      name: this.props.name,
      sport: this.props.sport
    });
    this.props.editAthleteEditState();
  }
}
