import React, {Component} from 'react';
import RenderRatingActions from './RenderRatingActions'

export default class RenderAthlete extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditRating: false
    }
  }

  render() {
    const {rating} = this.props;
    return (
      <tr data-id = {rating.ratingId}>
        <td>
          {rating.date}
        </td>
        <td>
          {rating.medal}
        </td>
        <RenderRatingActions
          deleteRating={this.props.deleteRating}
          editRating={this.props.editRating}
          editStateFunc={this.editStateFunc}
          rating = {rating}/>
      </tr>
    )
  }
}
