require('../../styles/Edit.css');

import React, {Component} from 'react';

export default class RenderActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      medal: 0,
      date: ''
    }
  }

  render() {
    return (
    <div className="container">
      <div className="col-center">
        <input type="date" className="form-control col-lg-3 col-lg-offset-3 add_name"
               defaultValue="1980-01-01"
               min="1980-10-01"
               max="2017-11-01"
               value={this.state.date || '1980-01-01'}
               onChange={this.handlegetDate.bind(this)}
               ref="createDate"
        />
        <br/>
        <div className="container">
          <label className="radio-inline">
            <input
               type="radio"
               name="survey"
               id="Radios1"
               value="3"
               ref="createMedal"
               defaultChecked={true}
               onChange={this.handleAddMedal.bind(this)}
            />
              Gold
          </label>
          <label className="radio-inline middle_radio">
            <input
              type="radio"
              name="survey"
              id="Radios2"
              value="2"
              ref="createMedal"
              onChange={this.handleAddMedal.bind(this)}
            />
              Silver
          </label>
          <label className="radio-inline">
            <input
              type="radio"
              name="survey"
              id="Radios3"
              value="1"
              ref="createMedal"
              onChange={this.handleAddMedal.bind(this)}
            />
              Bronze
          </label>
        </div>
        <br/>
        <button
          className="btn btn-success col-md-3"
          onClick={this.handleAddDate.bind(this)}
          type="button">
          Add
        </button>
      </div>
      <br/>
    </div>
    )
  }

  handlegetDate (ev) {
    this.setState({
      date: ev.target.value
    });
  }

  handleAddMedal(ev) {
    this.setState({
      medal: ev.target.value
    });
  }

  handleAddDate() {
    let DateComp = this.refs.createDate.value || this.refs.createDate.defaultValue;
    let Medal = this.state.medal || 3;

    let dat = Date.now();
    if (Date.parse(DateComp) < dat && 315522000000 < Date.parse(DateComp) ) {
      this.props.createRating(DateComp, Medal);
    } else {

      this.refs.createDate.classList.add('error');
    }
  }
}
