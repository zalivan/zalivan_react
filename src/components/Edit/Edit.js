import React from 'react'
import Calendar from './Calendar'
import EditAthlete from './EditAthlete'
import RenderConver from './RenderConver'

export default class Edit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      AllAthletes: JSON.parse(localStorage.getItem('AllAthletes')) || [],
      tempAthlete: {},
      isEdit: false,
      isEditAthlete: false,
      tempDate: '',
      medal : 0,
      date: ''
    }
  }

  createRating(tempDate, tempMedal) {
    const tempAthletes = this.state.AllAthletes;
    const tempId = Number(location.pathname.substr(1, 13));
    let temp;
    let tempRating = 0;

    tempAthletes.forEach((obj, index) => {
      if (obj.id === tempId) {
        temp = index;
      }
    });

    let RateObj = {
      date: tempDate,
      medal: tempMedal,
      ratingId: Date.now()
    };

    tempAthletes[temp].conv.push(RateObj);

    tempAthletes[temp].conv.forEach((rating) => {
      tempRating += +rating.medal;
    });

    tempAthletes[temp].rating = +(tempRating/tempAthletes[temp].conv.length).toFixed(1) || 0;

    localStorage.setItem('AllAthletes', JSON.stringify(tempAthletes));
    this.setState({
      AllAthletes: tempAthletes
    })
  }

  deleteRating(Id) {
    const tempAthletes = this.state.AllAthletes;
    const tempId = Number(location.pathname.substr(1, 13));
    let tempRating = 0;
    let temp;

    tempAthletes.forEach((obj, index) => {
      if (obj.id === tempId) {
        temp = index;
      }
    });

    tempAthletes[temp].conv = tempAthletes[temp].conv.filter((obj) => {
      if (obj.ratingId !== Id) return obj;
    });

    tempAthletes[temp].conv.forEach((rating) => {
      tempRating += +rating.medal;
    });

    tempAthletes[temp].rating = +(tempRating/tempAthletes[temp].conv.length).toFixed(1) || 0;

    localStorage.setItem('AllAthletes', JSON.stringify(tempAthletes));
    this.setState({
      AllAthletes: tempAthletes,
      isIs: false
    })
  }

  editRatingSet() {
    let DateComp = this.refs.changeDate.value || this.refs.createDate.defaultValue;
    let dat = Date.now();

    if (Date.parse(DateComp) < dat && 315522000000 < Date.parse(DateComp)) {
      const tempAthletes = this.state.AllAthletes;
      const tempId = Number(location.pathname.substr(1, 13));
      let tempRating = 0;
      let temp;
      let Id = this.state.tempDate;

      tempAthletes.forEach((obj, index) => {
        if (obj.id === tempId) {
          temp = index;
        }
      });

      tempRating = tempAthletes[temp].conv.findIndex((obj) => {
        if (obj.ratingId === Id) return obj;
      });

      tempAthletes[temp].conv[tempRating].date = this.refs.changeDate.value;
      tempAthletes[temp].conv[tempRating].medal = this.state.medal || this.refs.createMedal.defaultValue;

      tempAthletes[temp].conv.forEach((rating) => {
        tempRating += +rating.medal;
      });

      tempAthletes[temp].rating = +(tempRating / tempAthletes[temp].conv.length).toFixed(1) || 0;

      localStorage.setItem('AllAthletes', JSON.stringify(tempAthletes));
      this.setState({
        AllAthletes: tempAthletes,
        isIs: false
      })
    } else {
      document.getElementsByClassName('add_date')[0].classList.add('error');
    }
  }

  editRating(Id) {
    this.setState({
      isIs: true,
      tempDate: Id
    });
  }

  editAthlete(Name, Sport) {
    const tempAthletes = this.state.AllAthletes;
    const tempId = Number(location.pathname.substr(1, 13));
    let temp;

    tempAthletes.forEach((obj, index) => {
      if (obj.id === tempId) {
        temp = index;
      }
    });

    tempAthletes[temp].name = Name;
    tempAthletes[temp].sport = Sport;

    localStorage.setItem('AllAthletes', JSON.stringify(tempAthletes));
    this.setState({
      AllAthletes: tempAthletes,
      isEditAthlete: false
    })
  }

  editAthleteEditState() {
    this.setState({
      isEditAthlete: !this.state.isEditAthlete
    })
  }

  render() {
    const tempAthletes = this.state.AllAthletes;
    const tempId = Number(location.pathname.substr(1, 13));

    let tempAthlete = tempAthletes.filter((obj) => {
      if (obj.id === tempId) {
        return obj;
      }
    });

    let Temp = tempAthlete[0].conv.findIndex((obj) => {
      if (obj.ratingId === this.state.tempDate) return obj;
    });

    let calenderSelector = '';
    if (!this.state.isEditAthlete) {
      calenderSelector = (
        <div>
            <Calendar Athlete={tempAthlete[0]} createRating={this.createRating.bind(this)}/>
            <RenderConver
          athlete={tempAthlete[0]}
          deleteRating={this.deleteRating.bind(this)}
          editRating={this.editRating.bind(this)}
          />
        </div>
      )
    }

    let tempSelector = '';
    if ( !this.state.isIs ) {
      tempSelector = (
        <div>
          <EditAthlete
            Athlete={tempAthlete[0]}
            editAthlete={this.editAthlete.bind(this)}
            editAthleteEditState={this.editAthleteEditState.bind(this)}/>
          {calenderSelector}
        </div>
      )
    } else {
      tempSelector = (
        <div className="container">
          <div className="col-center">
            <input type="date" className="form-control col-lg-3 col-lg-offset-3 add_date"
                   // defaultValue={tempAthlete[0].conv[Temp].date}
                   min="1980-01-01"
                   max="2017-11-29"
                   value={this.state.date || tempAthlete[0].conv[Temp].date}
                   onChange={this.handlegetDate.bind(this)}
                   ref="changeDate"
            />
            <br/>
            <div className="container">
              <label className="radio-inline">
                <input
                  type="radio"
                  name="survey"
                  id="Radios1"
                  value="3"
                  defaultChecked={+tempAthlete[0].conv[Temp].medal === 3}
                  ref="createMedal"
                  onChange={this.handleAddMedal.bind(this)}
                />
                Gold
              </label>
              <label className="radio-inline middle_radio">
                <input
                  type="radio"
                  name="survey"
                  id="Radios2"
                  value="2"
                  defaultChecked={+tempAthlete[0].conv[Temp].medal === 2}
                  ref="createMedal"
                  onChange={this.handleAddMedal.bind(this)}
                />
                Silver
              </label>
              <label className="radio-inline">
                <input type="radio"
                       name="survey" id="Radios3"
                       value="1" ref="createMedal"
                       defaultChecked={+tempAthlete[0].conv[Temp].medal === 1}
                       onChange={this.handleAddMedal.bind(this)}
                />
                Bronze
              </label>
            </div>
            <br/>
            <button className="btn btn-success col-md-3 button_edit_bottom"
                    onClick={this.editRatingSet.bind(this)}
                    type="button">Ok</button>
            <button className="btn btn-warning col-md-3"
              onClick={this.handleCancelEditComp.bind(this)}
                    type="button">Cancel</button>
          </div>
          <br/>
        </div>
      )
    }

    return (
      <div>
        <h1>Athlete : {tempAthlete[0].name} ({tempAthlete[0].sport})</h1>
        <h1>Rating : {tempAthlete[0].rating}</h1>
        {tempSelector}

      </div>
    )
  }

  handleAddMedal(ev) {
    this.setState({
      medal: ev.target.value
    });
  }

  handlegetDate (ev) {
    this.setState({
      date: ev.target.value
    });
  }

  handleCancelEditComp=() => {
    this.setState({
      isIs: false
    })
  };
}
