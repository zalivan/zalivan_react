import React, {Component} from 'react';
import RenderRating from './RenderRating'

export default class RenderAthlete extends Component {

  render() {
    const {athlete} = this.props;
    const ratingElement = athlete.conv.map(rating => <RenderRating
      key={rating.ratingId} rating = {rating} deleteRating={this.props.deleteRating} editRating={this.props.editRating}/>);

    return (
      <table className="table">
        <tr>
          <td>Date</td>
          <td>Result</td>
          <td>Action</td>
        </tr>
        <tbody>
          {ratingElement}
        </tbody>
      </table>
    )
  }
}
