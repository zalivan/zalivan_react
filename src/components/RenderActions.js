require('styles/App.css');
import React, {Component} from 'react';
import { Link } from 'react-router-dom';

export default class RenderActions extends Component {

  render() {
    const {athlete} = this.props;
    return (
        <td>
          <Link to={athlete.id.toString()} className="btn btn-warning button_edit">
            Edit
          </Link>
          <button
            className="btn btn-danger"
            onClick={this.handleDelete.bind(this)}>
            Delete
          </button>
        </td>
    )
  }

  handleDelete = (ev) => {
    const idAthlete = ev.target.parentElement.parentElement.getAttribute('data-id');
    this.props.Delete(idAthlete);
  }
}
