import React, {Component} from 'react';
import RenderActions from './RenderActions'

export default class RenderAthlete extends Component {

  render() {
    const {athlete} = this.props;

    return (
      <tr data-id = {athlete.id}>
        <td>
          {athlete.name}
        </td>
        <td>
          {athlete.sport}
        </td>
        <td>
          {athlete.rating}
        </td>
        <RenderActions Delete = {this.props.Delete} athlete = {athlete}/>
      </tr>
    )
  }
}
