require('normalize.css/normalize.css');
require('styles/Table.css');

import React from 'react';
import RenderAthlete from './RenderAthlete';

export default function Table(props) {
  const {athletes} = props;

  let tempAthletes = athletes.sort(function (a, b) {
    if (a.rating > b.rating) {
      return -1;
    }
    if (a.rating < b.rating) {
      return 1;
    }
    return 0;
  });

  const athletesElement = tempAthletes.map(athlete => <RenderAthlete
    key={athlete.id} athlete = {athlete} Delete = {props.Delete}/>);

  return (
      <table className="table">
        <thead>
          <tr>
            <td>Name</td>
            <td>Sport</td>
            <td>Rating</td>
            <td>Actions</td>
          </tr>
        </thead>
        <tbody>
          {athletesElement }
        </tbody>
      </table>
  )
}
