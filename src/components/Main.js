require('normalize.css/normalize.css');
require('styles/App.css');

import React, { Component } from 'react';
import Table from './Table';
import Add from './Add';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      AllAthletes: JSON.parse(localStorage.getItem('AllAthletes')) || []
    }
  }

  Create(name, sport){
    let tempAthletes = this.state.AllAthletes;
    let Athlete = {
      name,
      sport,
      id: Date.now(),
      rating: 0,
      conv: []
    };

    tempAthletes.push(Athlete);

    localStorage.setItem('AllAthletes', JSON.stringify(tempAthletes));

    this.setState({
      AllAthletes: tempAthletes
    })
  }

  Delete(id) {
    let tempAllAthletes = this.state.AllAthletes.filter(function (obj) {
      if (obj.id !== +id) return obj;
    });

    localStorage.setItem('AllAthletes', JSON.stringify(tempAllAthletes));

    this.setState({
      AllAthletes: tempAllAthletes
    })
  }

  render () {
    return (
      <div>
        <div className="index">
          <h1 className="title">Add new athlete!</h1>
          <Add
            Create = {this.Create.bind(this)}
          />
        </div>
        <Table
          athletes={this.state.AllAthletes}
          Delete = {this.Delete.bind(this)}
        />
      </div>
    );
  }
}
