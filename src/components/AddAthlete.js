require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';

export default class AddAthlete extends React.Component {
  state = {
    name: '',
    sport: ''
  };

  render() {
    return(
      <div className="container">
          <div className="col-center">
            <input
               type="text" className="form-control col-lg-6 col-lg-offset-3 add_name"
               placeholder="Add sportsman's name"
               value={this.state.name}
               onKeyDown={this.keyDownEnter}
               onChange={this.handleAthleteName.bind(this)}
               ref = "createName"
               id="name_athlete"
            />
            <br/>
            <input
               type="text" className="form-control col-lg-6 col-lg-offset-3 add_sport"
               placeholder="Add kind of sport"
               ref = "createSport"
               onKeyDown={this.keyDownEnterSport}
               onChange={this.handleAthleteSport.bind(this)}
               value = {this.state.sport}
            />
            <br/>
            <button
              className="btn btn-success col-md-3"
              type="button"
              onClick={this.addAthleteFunc.bind(this)}>
              Add
            </button>
          </div>
        </div>
    );
  }

  keyDownEnter = (ev) => {
    if (ev.keyCode === 13) {
      this.addAthleteFunc()
    } else {
      this.setState({
        name: ev.target.value
      });
    }
  };

  keyDownEnterSport = (ev) => {
    if (ev.keyCode === 13) {
      this.addAthleteFunc()
    } else {
      this.setState({
        sport: ev.target.value
      });
    }
  };

  handleAthleteName (ev) {
    this.setState({
      name: ev.target.value
    });
  }

  handleAthleteSport (ev) {
    this.setState({
      sport: ev.target.value
    });
  }

  addAthleteFunc () {
    const Name = this.refs.createName;
    const Sport = this.refs.createSport;

    Name.classList.remove('error');
    Sport.classList.remove('error');

    if((Name.value.trim() === '')&&(Name.value.trim().replace(/\s/g, '') === '')) {
      Name.classList.add('error');
    } else if ((Sport.value.trim() === '')&&(Sport.value.trim().replace(/\s/g, '') === '')) {
      Sport.classList.add('error');
    } else {
      this.props.Create(Name.value.trim(), Sport.value.trim());
      this.setState({
        name: '',
        sport: ''
      })
    }
  }
}
