require('normalize.css/normalize.css');

import React, {Component} from 'react';
import AddAthlete from './AddAthlete';
import toggleOpen from '../decorators/toogleOpen'

class Add extends Component {
  render() {
    const {isOpen, toggleOpen, Create} = this.props;
    let tempAddAthlete;
    if (isOpen) {
      tempAddAthlete = <AddAthlete Create={Create}/>;
    }
    return (
      <div className="add">
        <button id="add_sportsman" className="btn btn-success" onClick={toggleOpen}>
          { !isOpen ? 'Add' : 'Cancel' }
        </button>
        {tempAddAthlete}
      </div>
    );
  }
}

export default toggleOpen(Add);
